# README #

This is the front end for the online DnD sheet.

### What is this repository for? ###

* Front end (HTML mark up, styling and client funcionality)
* 0.1

### How do I get set up? ###

* Deploy dnd folder in any webserver

### Contribution guidelines ###

* Fork or branch.
* Contribute
* Review code

### Who do I talk to? ###

* Pablo Nieto - panietoar@gmail.com (project leader)
* Andres Nieto - andres.nieto.arias@gmail.com (front end developer)