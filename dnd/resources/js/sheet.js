var characterInfo;
var character;

$.ajaxSetup({
  'beforeSend': function(xhr) {
    if (localStorage.getItem('userToken')) {
      xhr.setRequestHeader('Authorization',
            'Bearer ' + localStorage.getItem('userToken'));
    }
  }
});

/* logout
localStorage.removeItem('token');
userProfile = null;
window.location.href = "/";
*/

$(document).ready(function(){
	Character = Backbone.Epoxy.Model.extend({
		defaults:{
			playerName: null,
			name: null,
			level: 1,
			chClass: null,
			paragon: null,
			epic: null,
			totalXp: 0,
			race: null,
			size: null,
			age: null,
			gender: null,
			height: null,
			weight: null,
			alignment: null,
			deity: null,
			company: null,
			initBonus: 0,
			strength: 9, 
			dexterity: 9, 
			constitution: 9, 
			intelligence: 9, 
			wisdom: 9, 
			charisma: 9,
			baseHP: 9,
			classHP: 0,
			//Defenses
			acClassBonus: 0,
			acFeatBonus: 0,
			acEnhBonus: 0,
			acMiscAutoBonus: 0,
			acMiscBonus: 0,
			//Skills
			acroSkillTrained: false,
			arcaSkillTrained: false,
			athlSkillTrained: false,
			blufSkillTrained: false,
			diplSkillTrained: false,
			dungSkillTrained: false,
			enduSkillTrained: false,
			healSkillTrained: false,
			histSkillTrained: false,
			insiSkillTrained: false,
			intiSkillTrained: false,
			natuSkillTrained: false,
			percSkillTrained: false,
			reliSkillTrained: false,
			steaSkillTrained: false,
			streSkillTrained: false,
			thieSkillTrained: false
			//Equipment

		},
		computeds:{
			halfLevel: function(){
				return halfLevelModifier(this.get('level'));
			},
			tenPlusLevel: function(){
				return 10 + this.get('halfLevel');
			},
			initiative: function(){
				return this.get('dexMod') + this.get('halfLevel') +  parseInt(this.get('initBonus'));
			},
			classSpeed: function(){
				return this.get('race') ? this.get('race').speed : 0;
			},
			speed: function(){
				return this.get('classSpeed');
			},
			//Abilities
			strMod: function(){
				return abilityModifier(this.get('strength'));
			},
			strMod_lvl: function(){
				return this.get('strMod') + halfLevelModifier(this.get('level'));
			},
			dexMod: function(){
				return abilityModifier(this.get('dexterity'));
			},
			dexMod_lvl: function(){
				return this.get('dexMod') + halfLevelModifier(this.get('level'));
			},
			conMod: function(){
				return abilityModifier(this.get('constitution'));
			},
			conMod_lvl: function(){
				return this.get('conMod') + halfLevelModifier(this.get('level'));
			},
			intMod: function(){
				return abilityModifier(this.get('intelligence'));
			},
			intMod_lvl: function(){
				return this.get('intMod') + halfLevelModifier(this.get('level'));
			},
			wisMod: function(){
				return abilityModifier(this.get('wisdom'));
			},
			wisMod_lvl: function(){
				return this.get('wisMod') + halfLevelModifier(this.get('level'));
			},
			chaMod: function(){
				return abilityModifier(this.get('charisma'));
			},
			chaMod_lvl: function(){
				return this.get('chaMod') + halfLevelModifier(this.get('level'));
			},
			//Health
			maxHP: function(){
				return this.get('baseHP') + this.get('constitution') + ((this.get('level') - 1) * this.get('classHP'));
			},
			surgeValue: function(){
				return Math.floor(this.get('maxHP') / 4 );
			},
			//Defenses
			acArmorAbil: function(){
				return this.get('armor') ? this.get('armor').acBonus + (this.get('armor').type === 'L' ? this.get('reflexAbilBonus') : 0 ) : 0;
			},
			charAC: function(){
				return this.get('tenPlusLevel') + this.get('acArmorAbil') + this.get('acClassBonus') + this.get('acFeatBonus') + this.get('acEnhBonus') + this.get('acMiscAutoBonus') + parseInt(this.get('acMiscBonus'));
			},
			charFOR: function(){
				return this.get('tenPlusLevel') + this.get('fortitudeAbilBonus');
			},
			fortitudeAbilBonus: function(){
				return Math.max(this.get('strMod'), this.get('conMod'));
			},
			charREF: function(){
				return this.get('tenPlusLevel') + this.get('reflexAbilBonus');
			},
			reflexAbilBonus: function(){
				return Math.max(this.get('dexMod'), this.get('intMod'));
			},
			charWIL: function(){
				return this.get('tenPlusLevel') + this.get('willAbilBonus');
			},
			willAbilBonus: function(){
				return Math.max(this.get('wisMod'), this.get('chaMod'));
			},
			armorPenalty: function(){
				return 0;
			},
			//Skills
			acroSkill: function(){
				return this.get('dexMod') + this.get('halfLevel') + (this.get('acroSkillTrained') ? 5 : 0);
			},
			arcaSkill: function(){
				return this.get('intMod') + this.get('halfLevel') + (this.get('arcaSkillTrained') ? 5 : 0);
			},
			athlSkill: function(){
				return this.get('strMod') + this.get('halfLevel') + (this.get('athlSkillTrained') ? 5 : 0);
			},
			blufSkill: function(){
				return this.get('chaMod') + this.get('halfLevel') + (this.get('blufSkillTrained') ? 5 : 0);
			},
			diplSkill: function(){
				return this.get('chaMod') + this.get('halfLevel') + (this.get('diplSkillTrained') ? 5 : 0);
			},
			dungSkill: function(){
				return this.get('wisMod') + this.get('halfLevel') + (this.get('dungSkillTrained') ? 5 : 0);
			},
			enduSkill: function(){
				return this.get('conMod') + this.get('halfLevel') + (this.get('enduSkillTrained') ? 5 : 0);
			},
			healSkill: function(){
				return this.get('wisMod') + this.get('halfLevel') + (this.get('healSkillTrained') ? 5 : 0);
			},
			hisAutoBonus: function(){
				if(this.get('race')){
					return this.get('race').skillBonus ? this.get('race').skillBonus.historyBonus : 0;
				}
			},
			histSkill: function(){
				return this.get('intMod') + this.get('halfLevel') + (this.get('histSkillTrained') ? 5 : 0) + parseInt(this.get('hisAutoBonus'));
			},
			insiSkill: function(){
				return this.get('wisMod') + this.get('halfLevel') + (this.get('insiSkillTrained') ? 5 : 0);
			},
			intiSkill: function(){
				return this.get('chaMod') + this.get('halfLevel') + (this.get('intiSkillTrained') ? 5 : 0);
			},
			natuSkill: function(){
				return this.get('wisMod') + this.get('halfLevel') + (this.get('natuSkillTrained') ? 5 : 0);
			},
			percSkill: function(){
				return this.get('wisMod') + this.get('halfLevel') + (this.get('percSkillTrained') ? 5 : 0);
			},
			reliSkill: function(){
				return this.get('intMod') + this.get('halfLevel') + (this.get('reliSkillTrained') ? 5 : 0);
			},
			steaSkill: function(){
				return this.get('dexMod') + this.get('halfLevel') + (this.get('steaSkillTrained') ? 5 : 0);
			},
			streSkill: function(){
				return this.get('chaMod') + this.get('halfLevel') + (this.get('streSkillTrained') ? 5 : 0);
			},
			thieSkill: function(){
				return this.get('dexMod') + this.get('halfLevel') + (this.get('thieSkillTrained') ? 5 : 0);
			}
		}

	});
	
	$.ajax({
		method: "GET",
		url: "http://mydndapi.azurewebsites.net/api/character/Griff/" //Change url in live
	}).done(function(data){
		loadCharacter(JSON.parse(data));
	});

	loadLists();
	$("#char_race").on('change', function(){
		changeRace($(this).val());
	});
});

function loadCharacter(data){
	characterInfo = data;
	character = new Character();
	character.set(data);
	var view = new Backbone.Epoxy.View({
		el: "#sheet_body",
		model: character
	});
}

function abilityModifier(abilityScore){
	return Math.floor((abilityScore - 10) / 2);
}

function halfLevelModifier(level){
	return Math.floor(level / 2);
}

function loadLists(){
	$.ajax({
		method: "GET",
		url: "json/races.json"
	}).done(function(data){
		var cantidad = data.totalResults;
		var resultados = data.results;
		$.each(resultados, function(index, raza){
			$("#char_race").append("<option value='"+raza.value+"' "+ (raza.name === characterInfo.race ? "selected": "") +">"+raza.name+"</option>");
		});
	});
}

function changeRace(newRace){
	$.ajax({
		method: 'GET',
		url: "json/" + newRace + ".json"
	}).done(function(data){
		 character.set(data);
	});
}